<?php

namespace App\Http\Controllers;

use App\Jobs\ProcessVideo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Queue;
use Laravel\Lumen\Routing\Controller as BaseController;

class ApiController extends BaseController
{
    public function processVideo(Request $request)
    {
        $this->validate($request, [
            'torrent_id'    => 'required',
            'torrent_name'  => 'required',
            'torrent_dir'   => 'required',
            'callback_url'  => 'url'
        ]);

        $torrent_id     = $request->torrent_id;
        $torrent_name   = $request->torrent_name;
        $torrent_dir    = $request->torrent_dir;
        $callback_url   = $request->callback_url;

        $job_id         = Queue::push(new ProcessVideo($torrent_id, $torrent_name, $torrent_dir, $callback_url));
        $job            = DB::table('jobs')->where('id', $job_id)->first();

        return response()
            ->json($job)
            ->setStatusCode(201);
    }

    public function showJob($id)
    {
        $job                = DB::table('jobs')->where('id', $id)->first();
        $successful_job     = DB::table('successful_jobs')->where('id', $id)->first();

        if(is_null($job) && is_null($successful_job)) {
            abort(404);
        }
        return $job ? json_encode($job) : json_encode($successful_job);
    }
}
