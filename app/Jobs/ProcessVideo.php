<?php

namespace App\Jobs;

use App\Jobs\Notifications\JobFailed;
use App\Jobs\Notifications\JobFinished;
use App\Jobs\Notifications\JobStarted;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

/**
 * Class ProcessVideo
 * @package App\Jobs
 */
class ProcessVideo extends Job
{
    private $torrent_id;
    private $torrent_name;
    private $torrent_dir;
    private $callback_url;

    /**
     * Create a new job instance.
     *
     * @param $torrent_id
     * @param $torrent_name
     * @param $torrent_dir
     * @param null $callback_url
     */
    public function __construct($torrent_id, $torrent_name, $torrent_dir, $callback_url = null)
    {
        $this->torrent_id   = $torrent_id;
        $this->torrent_name = $torrent_name;
        $this->torrent_dir  = $torrent_dir;
        $this->callback_url = $callback_url;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        dispatch(new JobStarted(
            $this->getTorrentId(),
            'started',
            $this->getCallbackUrl()
        ));

        $process = new Process([
            '/data/process-download.su',
            $this->getTorrentId(),
            $this->getTorrentName(),
            $this->getTorrentDir()
        ]);

        $process->setTimeout(3600);
        $process->run();

        // executes after the command finishes
        if (!$process->isSuccessful()) {

            dispatch(new JobFailed(
                $this->getTorrentId(),
                $process->getErrorOutput(),
                $this->getCallbackUrl()
            ));

            throw new ProcessFailedException($process);
        }

        $output = $process->getOutput();

        dispatch(new JobFinished(
            $this->getTorrentId(),
            $output,
            $this->getCallbackUrl()
        ));

        $job_id = $this->job->getJobId();

        DB::insert('insert into successful_jobs (id, output) values (?, ?)', [
            $job_id,
            $output
        ]);
    }

    /**
     * @return mixed
     */
    public function getTorrentId()
    {
        return $this->torrent_id;
    }

    /**
     * @return mixed
     */
    public function getTorrentName()
    {
        return $this->torrent_name;
    }

    /**
     * @return mixed
     */
    public function getTorrentDir()
    {
        return $this->torrent_dir;
    }

    /**
     * @return null
     */
    public function getCallbackUrl()
    {
        return $this->callback_url;
    }
}
