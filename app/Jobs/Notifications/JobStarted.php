<?php

namespace App\Jobs\Notifications;

class JobStarted extends JobStatusChanged
{
    public function getStatus()
    {
        return 'started';
    }
}
