<?php

namespace App\Jobs\Notifications;

class JobFailed extends JobStatusChanged
{
    public function getStatus()
    {
        return 'failed';
    }
}
