<?php

namespace App\Jobs\Notifications;

use App\Jobs\Job;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

/**
 * Class JobStatusChanged
 * @package App\Jobs\Notifications
 */
abstract class JobStatusChanged extends Job
{
    /**
     * @var string
     */
    protected $id;
    /**
     * @var string
     */
    protected $callbackUrl;
    /**
     * @var string
     */
    protected $error;

    /**
     * Create a new job instance.
     *
     * @param string $id
     * @param string $error
     * @param string|null $callbackUrl
     */
    public function __construct(string $id, string $error, ?string $callbackUrl)
    {
        $this->id           = $id;
        $this->error        = $error;
        $this->callbackUrl  = $callbackUrl;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        if(is_null($this->getCallbackUrl())) {
            return ;
        }

        $client = new Client();

        $response = $client->get($this->getCallbackUrl(), [
            'query' => [
                'id'        => $this->getId(),
                'status'    => $this->getStatus(),
                'response'  => json_encode($this->getError())
            ]
        ]);

        Log::info($response->getStatusCode());
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCallbackUrl(): ?string
    {
        return $this->callbackUrl;
    }

    /**
     * @return string
     */
    public function getError(): string
    {
        return $this->error;
    }

    abstract function getStatus();
}
