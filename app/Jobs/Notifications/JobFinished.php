<?php

namespace App\Jobs\Notifications;

class JobFinished extends JobStatusChanged
{
    public function getStatus()
    {
        return 'finished';
    }
}
